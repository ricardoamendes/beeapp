
preferred_syntax = :sass
http_path = ''
sass_dir = 'css'
images_dir = (environment == :production) ? 'build/svg' : 'res/svg'
fonts_dir = (environment == :production) ? 'app/res/fonts' : 'res/fonts'
asset_cache_buster :none;
relative_assets = (environment == :production) ? false : true
css_dir = (environment == :production) ? 'build/css' : 'css'
output_style = (environment == :production) ? :compressed : :expanded
line_comments = (environment == :production) ? false : true

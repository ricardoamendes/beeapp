/**
 * Main store holding the global state and data for React components.
 */

// Utils
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

// App action dispatcher and types
var Dispatcher = require('../dispatcher/AppDispatcher');
var ActionTypes = require('../constants/ActionTypes').ActionTypes;

// Enable up to 20 listeners
EventEmitter.EventEmitter.defaultMaxListeners = 20;

var AppStore = assign({ }, EventEmitter.prototype, {
    /**
     * Hold the page details for an app - title and blurb.
     */
    _details: null,
    /**
     * Determine if the Details page should be visible or not.
     */
    _showDetails: false,
    /**
     * Determine if the Nav Bar should be visible or not.
     */
    _showNavBar: false,
    /**
     * Hold the next page index.
     */
    _slideIndex: 0,
    /**
     * Emitter helper to send events to the Views.
     */
    emitChange: function(event) {
        this.emit(event);
    },
    /**
     * Helper to attach an event handler.
     */
    addChangeListener: function(event, callback) {
        this.on(event, callback);
    },
    /**
     * Helper to remove an event handler.
     */
    removeChangeListener: function(event, callback) {
        this.removeListener(event, callback);
    },

    /**
     * Setters and Getters
     */

    setDetails: function(details) {
        this._details = details;
    },
    getDetails: function() {
        return this._details;
    },

    setShowDetails: function(show) {
        this._showDetails = show;
    },
    getShowDetails: function() {
        return this._showDetails;
    },

    setShowNavBar: function(show) {
        this._showNavBar = show;
    },
    getShowNavBar: function() {
        return this._showNavBar;
    },

    setSlideIndex: function(index) {
        this._slideIndex = index;
    },
    getSlideIndex: function() {
        return this._slideIndex;
    }
});

AppStore.dispatchToken = Dispatcher.register(function(action) {
    switch (action.actionType) {
        // assign details for an app and display/hide the page
        case ActionTypes.SHOW_DETAILS:
            if (action.details) {
                AppStore.setDetails(action.details);
                AppStore.setSlideIndex(2);
                AppStore.setShowDetails(true);
            } else {
                AppStore.setShowDetails(false);
            }
            AppStore.emitChange(ActionTypes.SHOW_DETAILS);
            break;

        // update the nav bar visibility
        case ActionTypes.SHOW_NAV_BAR:
            AppStore.setShowNavBar(action.show);
            AppStore.emitChange(ActionTypes.SHOW_NAV_BAR);
            break;

        // dispatch a swiping action to a new page
        case ActionTypes.SLIDE:
            AppStore.setSlideIndex(action.index);
            AppStore.emitChange(ActionTypes.SLIDE);
            break;
        default:
    // do nothing
    }
});

module.exports = AppStore;

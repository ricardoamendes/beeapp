/*
 * A singleton that operates as the central hub for updates across the app.
 */

var AppDispatcher = require('flux').Dispatcher;

module.exports = new AppDispatcher();

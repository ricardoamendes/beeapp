/**
 * This class will be responsible to bootstrap and expose the functionality
 * required in the global level. The main component <Main/> will be mounted
 * in the DOM as the entry point.
 */

require("babel-polyfill");

// React
import React from 'react';

// Config and 3rd parties
import config from './config';
import Parse from 'parse';
import AnimationFrame from 'animation-frame';
import Sniffer from 'snifferjs/dist/sniffer';

// Components
import Main from './components/Main.jsx';
import AboutMe from './components/AboutMe.jsx';
import Apps from './components/Apps.jsx';
import Details from './components/Details.jsx';

// Actions
import AppActions from './actions/AppActions';

// Create an empty global
window.beeapp = {};

// Expose RAF helper
window.beeapp.RAF = new AnimationFrame();

// Expose device/browser info
window.beeapp.sniffer = Sniffer;

// Disables browser scrolling defaults
document.addEventListener('touchmove', function(e){e.preventDefault();}, false);

// Initialize Parse interface
Parse.Parse.initialize(config.parse.appId, config.parse.appKey);

// App entry point
React.render(<Main/>, document.querySelector('#app'));

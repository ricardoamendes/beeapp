/**
 * Tile Component.
 */

// React
import React from 'react';
import cx from 'react/lib/cx';

// Actions and Store
import AppActions from '../actions/AppActions';
import Tappable from 'react-tappable';

// Template
import TileTpl from '../../templates/tile.rt';

class Tile extends React.Component {
    /**
	 * Set props, state and classes.
	 */
    constructor(props) {
        super(props);

        // State
        this.state = {
            visited: false
        };

        // Class names
        this.cls = {
            tile: 'tile',
            tileInner: 'tile__inner'
        };
    }
    /**
     * Dispatch an action to show details for given tile.
     * In first place applies the CSS scaling transform and then slides in to the Details.
     *
     * @param {Event} evt Event info
     * @return {void}
     */
    onClick(evt) {

        // handle touch/click events for mobile and desktop versions
        if (window.beeapp.sniffer.features.mobile || !window.beeapp.sniffer.features.mobile && evt.type === 'mouseup') {

            this.setState({ visited: true });

            AppActions.showDetails(this.props.details);
        }
    }
    /**
	 * Renders the Tile component.
	 */
    render() {

        Object.assign(this.cls, {
            tileVisited: this.state.visited ? 'tile--visited' : ''
        });

        return TileTpl.apply(this);
    }
}

export default Tile;

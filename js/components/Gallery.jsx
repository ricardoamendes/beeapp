/**
 * Gallery component.
 */

// React
import React from 'react';
import cx from 'react/lib/cx';

// Actions and Store
import AppStore from '../stores/AppStore';

// Template
import GalleryTpl from '../../templates/gallery.rt';

class Gallery extends React.Component {
    /**
	 * Set props, state and classes.
	 */
    constructor(props) {

        super(props);

        // State
        this.state = {
            isInitialized: false,
            renderImages: false
        };

        // Class names
        this.cls = {
            gallery: 'gallery',
            galleryIcon: 'gallery__icon',
            galleryLink: 'gallery__link',
            galleryIconImg: 'gallery__icon__image'
        };

    }
    /**
     * The first time is mounted preload gallery images.
     */
    componentDidMount() {
        if (!this.state.isInitialized) {
            this.preloadImages();
        }
    }
    /**
     * When the component is re-rendered update the gallery images.
     */
    componentDidUpdate() {
        // avoid preloading images twice if executed before when component was mounted
        if (!this.state.isInitialized || this.state.renderImages) {
            this.state.isInitialized = true;
            this.state.renderImages = false;
            // recalculate the iScroll third-party scroller as the gallery height is set
            this.props.scope.refs.iScroll['_iScrollInstance'].refresh();    // jshint ignore:line
        } else {
            this.preloadImages();
        }
    }
    /**
     * Avoid component updates if page is not displayed.
     */
    shouldComponentUpdate() {
        if (AppStore.getSlideIndex() !== 2) {
            // when the page transition finishes scroll contents to the top
            // Note: not written in dot notation due to an issue relating the module builder
            setTimeout(() => this.props.scope.refs.iScroll['_iScrollInstance'].scrollTo(0, 0), this.props.SLIDE_TRANSITION_MS); // jshint ignore:line
            return false;
        } else {
            return true;
        }
    }
    /**
     * Render the gallery component template view.
     */
    render() {

        Object.assign(this.cls, {
            galleryIconSpinner: !this.state.renderImages ? 'spinner' : '',
            galleryIconImageShow: this.state.renderImages ? 'gallery__icon__image--show' : ''
        });

        this.galleryLoopCount = [];

        for(let i = 1, len = Number(this.props.paths.count); i <= len; i++) {
           this.galleryLoopCount.push(i.toString());
        }

        return GalleryTpl.apply(this);
    }
    /**
     * Wait until all images are loaded.
     */
    preloadImages() {
        var imgs = document.querySelectorAll('.' + this.cls.galleryIconImg),
            imgLen = imgs.length,
            imgCount = 0,
            updateState = () => {
                this.state.renderImages = true;
                this.setState({
                    renderImages: true
                });
            },
            loadHandler = (e) => {
                imgCount += 1;
                if (imgCount === imgLen) {
                    updateState();
                }
                // unbind itself to force one single event
                e.target.removeEventListener(e.type, loadHandler); // jshint ignore:line
            };

        // set up a load listener for each img tag
        for (let i = 0; i < imgLen; ++i) {

            // check to see if the image is finished loading:
            if (imgs[i].complete) {
                updateState();
                break;
            }

            imgs[i].addEventListener('load', loadHandler);
        }
    }
}

// React getDefaultProps is never called when using ES6 class syntax.
// https://github.com/facebook/react/issues/3725
Gallery.defaultProps = {
    SLIDE_TRANSITION_MS: 500
};

module.exports = Gallery;

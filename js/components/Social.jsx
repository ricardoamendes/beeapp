/**
 * Social Item component.
 */

// React
import React from 'react';

// Template
import SocialTpl from '../../templates/social.rt';

class Social extends React.Component {
    /**
     * Set props, state and classes.
     */
    constructor(props) {
        super(props);

        // Class names
        this.cls = {
            social: 'social',
            socialIcon: 'social__icon',
            socialLink: 'social__icon__link'
        };
    }
    /**
  	 * Render the social component template view.
  	 */
    render(){
        return SocialTpl.apply(this);
    }
}

module.exports = Social;

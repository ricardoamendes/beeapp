/**
 * Main Class.
 */

// React
import React from 'react';

// Components
import AboutMe from './AboutMe.jsx';
import Apps from './Apps.jsx';
import Details from './Details.jsx';
import NavBar from './NavBar.jsx';

// Actions and Store
import AppActions from '../actions/AppActions';
import ActionTypes from '../constants/ActionTypes';
import AppStore from '../stores/AppStore';

// Template
import MainTpl from '../../templates/main.rt';

class Main extends React.Component {
    /**
	 * Set props, state and classes.
	 */
    constructor(props) {

        super(props);

        // State
        this.state = {
            activePageIndex: 0,
            screenWidth: window.innerWidth
        };

        // Class names
        this.cls = {
            main: 'main'
        };

        // Handle screen dimensions updates e.g. orientation changes
        window.addEventListener('resize', () => {
            this.setState({
                screenWidth: window.innerWidth
            });
        }, false);
    }
    /**
     * Set up listeners and the initial state of the app.
     */
    componentDidMount() {
        var main = React.findDOMNode(this).querySelector('.main'),
            pageData;

        // set store listeners
        AppStore.addChangeListener(ActionTypes.ActionTypes.SHOW_DETAILS, this.onPageSlide.bind(this));
        AppStore.addChangeListener(ActionTypes.ActionTypes.SLIDE, this.onPageSlide.bind(this));

        // apply slide in transform when rendered
        AppActions.showNavBar(true);
    }
    /**
     * Mount and render the main components <AboutMe/>, <Apps/> and <Details/>.
     */
    render() {
        this.screenWidth = this.state.screenWidth;
        this.offset = -(this.screenWidth * this.state.activePageIndex);
        this.inlineStyle = {
            width: (this.screenWidth * this.props.PAGE_COUNT) + 'px',
            WebkitTransform: 'translate(' + this.offset + 'px, 0)' + 'translateZ(0)',
            msTransform: 'translate(' + this.offset + 'px, 0)' + 'translateZ(0)',
            transform: 'translate(' + this.offset + 'px, 0)' + 'translateZ(0)'
        };

        return MainTpl.apply(this);
    }
    /**
     * Event handler to re-render the main component as a page slides.
     */
    onPageSlide() {
        this.setState({
            activePageIndex: AppStore.getSlideIndex()
        });
    }
}

// React getDefaultProps is never called when using ES6 class syntax.
// https://github.com/facebook/react/issues/3725
Main.defaultProps = {
    PAGE_COUNT: 3
};

export default Main;

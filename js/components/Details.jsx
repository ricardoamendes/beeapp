/**
 * Details Component.
 */

// React
import React from 'react';
import cx from 'react/lib/cx';

// 3rd-parties
import ReactIScroll from 'react-iscroll';
import iScroll from 'iscroll/build/iscroll';

// Actions and Store
import AppStore from '../stores/AppStore';

// Components
import DetailsTpl from '../../templates/details.rt';

class Details extends React.Component {
    /**
	 * Set props, state and classes.
	 */
    constructor(props) {

        super(props);

        // State
        this.state = {
            details: AppStore.getDetails()
        };

        // Class names
        this.cls = {
            details: 'details',
            detailsHeader: 'details__header',
            detailsHeaderTitle: 'details__header__title',
            detailsInnerBody: 'details__inner__body'
        };

        // Hold iScroll module
        this.iScroll = iScroll;
    }
    /**
     * Renders the Apps page template view
     */
    render() {
        var style = {
            width: '100%' // set defaults to fit screen
        };

        this.details = AppStore.getDetails();

        if (this.details) {
            return DetailsTpl.apply(this);
        } else {
            return (
                <div style={style}></div>
            );
        }
    }
}

// React getDefaultProps is never called when using ES6 class syntax.
// https://github.com/facebook/react/issues/3725
Details.defaultProps = {
    options: {
        click: true,
        shrinkScrollbars: 'scale'
    }
};

module.exports = Details;

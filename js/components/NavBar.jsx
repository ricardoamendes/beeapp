/**
 * NavBar Component.
 */

 // React
import React from 'react';
import cx from 'react/lib/cx';

// Actions and Store
import AppActions from '../actions/AppActions';
import ActionTypes from '../constants/ActionTypes';
import AppStore from '../stores/AppStore';

// Template
import NavBarTpl from '../../templates/navbar.rt';

class NavBar extends React.Component {
    /**
	 * Set props, state and classes.
	 */
    constructor(props) {

        super(props);

        // State
        this.state = {
            pages: [
                'About Me',
                'Apps',
                'Details'
            ],
            activePageIndex: 0,
            isDetailsHidden: true,
            collapsed: true,
            transform: 33
        };

        // Class names
        this.cls = {
            navBar: 'nav__bar',
            navBarInner: 'nav__bar__inner'
        };

    }
    /**
     * Bind listeners to handle the Nav Bar transitions and visibility.
     */
    componentDidMount() {
        AppStore.addChangeListener(ActionTypes.ActionTypes.SHOW_NAV_BAR, this.onShowNavBar.bind(this));
        AppStore.addChangeListener(ActionTypes.ActionTypes.SHOW_DETAILS, this.onPageSlide.bind(this));
        // enable onTouchStart events in the template view. This will allow a responsive and quicker
        // interaction while tapping over the Nav Bar titles.
        React.initializeTouchEvents(true);
    }
    /**
	 * Renders the NavBar component template view
	 */
    render() {

        // Class names
        this.cls = Object.assign(this.cls, {
            navBarCollapsed: this.state.collapsed ? 'nav__bar--collapse' : ''
        });

        this.activePageIndex = this.state.activePageIndex;
        this.isDetailsHidden = this.state.isDetailsHidden;
        this.onClickFn = this.onClick.bind(this);

        return NavBarTpl.apply(this);
    }
    /**
     * Event handler show/hide the nav bar.
     */
    onShowNavBar() {
        this.setState({
            collapsed: !AppStore.getShowNavBar()
        });
    }
    /**
     * Dispatches an action to show Details for given tile.
     *
     * @param {Event} evt Event info
     * @param {Object} keyRef Tapped key reference
     * @return {void}
     */
    onClick(evt, keyRef) {

        // handle touch/click events for mobile and desktop versions
        if (window.beeapp.sniffer.features.mobile && evt.type === 'touchstart' ||
            !window.beeapp.sniffer.features.mobile && evt.type === 'click'){

          // extract page index from react key
          let pageIndex = Number(event.target.dataset.index);

          // update nav bar state
          this.setState({
              activePageIndex: pageIndex,
              isDetailsHidden: pageIndex !== 2
          });

          AppActions.slideTo(pageIndex);
      }
    }
    /**
     * Displays the details title as its page finishes sliding.
     */
    onPageSlide() {
        // update nav bar state
        this.setState({
            activePageIndex: AppStore.getSlideIndex(),
            isDetailsHidden: AppStore.getSlideIndex() !== 2
        });
    }
}

// React getDefaultProps is never called when using ES6 class syntax.
// https://github.com/facebook/react/issues/3725
NavBar.defaultProps = {
    PAGE_FACTOR_OFFSET: 33
};

module.exports = NavBar;

/**
 * About Me component.
 */

// React
import React from 'react';
import cx from 'react/lib/cx';

// Parse
import ReactMixin from 'react-mixin';
import Parse from 'parse';
import ParseReact from 'parse-react';

// 3rd-parties
import ReactIScroll from 'react-iscroll';
import iScroll from 'iscroll/build/iscroll';

// Components
import Social from './Social.jsx';

// Actions and Store
import AppActions from '../actions/AppActions';

// Components
import AboutMeTpl from '../../templates/aboutme.rt';

class AboutMe extends React.Component {
    /**
     * Set props, state and classes.
     */
    constructor(props) {

        super(props);

        this.state = {
            isFooterDisplayed: true
        };

        // Class names
        this.cls = {
            aboutMe: 'aboutme',
            aboutMeHeader: 'aboutme__header',
            aboutMeHeaderLogo: 'aboutme__header__logo',
            aboutMeIntro: 'aboutme__intro',
            aboutMeFooter: 'aboutme__footer'
        };

        // Hold the number of sections returned by Parse query.
        this.cacheSectionsCount = 0;

        // Hold iScroll module
        this.iScroll = iScroll;
    }
    /**
     * Subscribe to all Parse AboutMe objects or else return cached objects.
     */
    observe() {
        return {
            sections: (new Parse.Parse.Query('AboutMe').ascending("order"))
        };
    }
    /**
     * Caches server data and recalculates the parallax scene height that relies on the scroller height.
     */
    componentDidUpdate() {
        var scroller,
            scene,
            sectionsCount = this.data.sections.length;

        // diff count and assigns new height to the scene and local storage
        if (sectionsCount !== this.cacheSectionsCount) {
            localStorage.setItem('aboutmeParseData', JSON.stringify(this.data.sections));
        }
    }
    /**
     * Renders the about me page template view.
     */
    render() {

        Object.assign(this.cls, {
            aboutMeFooterCollapse: !this.state.isFooterDisplayed ? 'aboutme__footer--collapse' : ''
        });

        // pre load from cache and once Parse retrieve remote contents let React calc any DOM diff changes
        this.sections = this.data.sections.length === 0 && this.props.cachedParseData ? this.props.cachedParseData : this.data.sections;

        return AboutMeTpl.apply(this);
    }
    /**
     * Hold the iScroll instance and show/hide the footer element as the viewport scroll starts.
     */
    onScrollStart(iScroll) {

        // prevent when swiping
        if (Math.abs(iScroll.distX) > Math.abs(iScroll.distY)) {
            return;
        }

        this.setState({
            isFooterDisplayed: false
        });
        AppActions.showNavBar(false);
    }
    /**
     * Show/hide footer as the viewport scroll ends.
     */
    onScrollEnd(iScroll) {
        this.setState({
            isFooterDisplayed: true
        });
        AppActions.showNavBar(true);
    }
}

// React getDefaultProps is never called when using ES6 class syntax.
// https://github.com/facebook/react/issues/3725
AboutMe.defaultProps = {
    options: {
        click: true,
        shrinkScrollbars: 'scale'
    },
    cachedParseData: JSON.parse(localStorage.getItem('aboutmeParseData')),
    intro: 'Welcome to my mobile portfolio! I’m Ricardo Mendes, a Mobile Developer based in Dublin. For the best experience try beeapp.io from a mobile device.',
    social: [{
        name: 'linkedin',
        url: 'https://ie.linkedin.com/pub/ricardo-mendes/13/b63/a94'
    }, {
        name: 'twitter',
        url: 'https://twitter.com/ricardoaomendes'
    }, {
        name: 'facebook',
        url: 'https://www.facebook.com/ricardolobomendes'
    }, {
        name: 'googleplus',
        url: 'https://plus.google.com/111900504282712715414/posts'
    }]
};

// Enable Parse query subscriptions
ReactMixin(AboutMe.prototype, ParseReact.Mixin);

module.exports = AboutMe;

/**
 * Apps Component.
 */

 // React
 import React from 'react';

 // Parse
 import ReactMixin from 'react-mixin';
 import Parse from 'parse';
 import ParseReact from 'parse-react';

 // 3rd-parties
 import ReactIScroll from 'react-iscroll';
 import iScroll from 'iscroll/build/iscroll';

// Components
import Tile from './Tile.jsx';

// Components
import AppsTpl from '../../templates/apps.rt';

class Apps extends React.Component {
    /**
	 * Set props, state and classes.
	 */
    constructor(props) {

        super(props);

        // Class names
        this.cls = {
            apps: 'apps'
        };

        // Hold iScroll module
        this.iScroll = iScroll;
    }
    /**
     * Subscribe to all Parse Apps objects.
     */
    observe() {
        return {
            apps: (new Parse.Parse.Query('Apps').ascending("order"))
        };
    }
    /**
     * Store in local storage Parse data.
     */
    componentDidUpdate() {
        var name,
            paths;

        localStorage.setItem('appsParseData', JSON.stringify(this.data.apps));
    }
    /**
     * Renders the Apps page template view
     */
    render() {

        // pre load from cache and once Parse retrieve contents from the server let React diff the DOM
        this.apps = this.data.apps.length === 0 && this.props.cachedParseData ? this.props.cachedParseData : this.data.apps;

        return AppsTpl.apply(this);
    }
    /**
     * Generate tile templates by 2 vs 3 items per row
     */
    renderTemplate(apps) {
        var me = this,
            renderRow = false,
            renderTiles = [],
            tpl;

        return apps.map((item, index, apps) => {

            // reset the array to process the next row
            if (renderRow) {
                renderRow = false;
                renderTiles = [];
            }

            renderTiles.push(item);

            // verify when a new row should be generated
            if ((index + 1).toString().match(/2$|5$|7$|0$/g) || index === apps.length - 1) {
                renderRow = true;
                return (
                    <div key={ index }>
                        { renderTiles.map((appDetail) => {
                              return (
                                  <Tile key={ appDetail.objectId } details={ appDetail } />
                                  );
                          }) }
                    </div>
                    );
            }
        });
    }
}

// React getDefaultProps is never called when using ES6 class syntax.
// https://github.com/facebook/react/issues/3725
Apps.defaultProps = {
    options: {
        click: true,
        shrinkScrollbars: 'scale'
    },
    cachedParseData: JSON.parse(localStorage.getItem('appsParseData'))
};

// Enable Parse query subscriptions
ReactMixin(Apps.prototype, ParseReact.Mixin);

module.exports = Apps;

/**
 * List of action types.
 */

var keyMirror = require('keymirror');

module.exports = {

    ActionTypes: keyMirror({
        SHOW_DETAILS: null,
        SHOW_NAV_BAR: null,
        SLIDE: null
    }),

};

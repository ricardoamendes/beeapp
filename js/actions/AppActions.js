/**
 * App Actions
 */

// Global action dispatcher 
var Dispatcher = require('../dispatcher/AppDispatcher');

// Action constants
var ActionTypes = require('../constants/ActionTypes').ActionTypes;

var AppActions = {
    /**
     * Show the Details page and pass the app title and blurbs.
     */
    showDetails: function(details) {
        Dispatcher.dispatch({
            actionType: ActionTypes.SHOW_DETAILS,
            details: details
        });
    },
    /**
     * Order to show/hide the navigation bar.
     */
    showNavBar: function(show) {
        Dispatcher.dispatch({
            actionType: ActionTypes.SHOW_NAV_BAR,
            show: show
        });
    },
    /**
     * Action to slide in to a new page.
     */
    slideTo: function(index) {
        Dispatcher.dispatch({
            actionType: ActionTypes.SLIDE,
            index: index
        });
    }
};

module.exports = AppActions;

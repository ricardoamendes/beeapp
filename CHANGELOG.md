# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [0.10.0] - 2015-09-17
### Changed
- Discarded the [Swipe](https://github.com/jed/react-swipe) third-party that enabled swipes gestures to navigate through different pages.
- Apply inline styling e.g. transform, width, only during React's render life cycle to batch multiple updates and avoid expensive layout recalculations.
- Improved and cleaned up the code as we removed Swipe.

### Fixed
- Using Swipe on top of [iScroll](https://github.com/schovi/react-iscroll) raised several gesture conflicts on more constrained Android devices. The workaround for the time being was disabling Swipe. We will carry out more experiments and create a POC with a custom Swipe component.

## [0.9.1] - 2015-09-16
### Added
- Add regex to redirect the app to the desktop landing page when not running over a mobile device ([Source](http://detectmobilebrowsers.com/)).

## [0.9.0] - 2015-09-15
### Added
- Inline styles for content above the fold as per page speed insights recommendations. Refer to [Reduce the size of the above-the-fold content](https://developers.google.com/speed/docs/insights/PrioritizeVisibleContent?hl=en) for more details.
- Logic to prevent render-blocking scripts. See more in [Remove Render-Blocking JavaScript](https://developers.google.com/speed/docs/insights/BlockingJS).
- Support for tappable react elements. See more in [react-tappable](https://github.com/JedWatson/react-tappable).

### Changed
- Intro text in the **About Me** component.
- Disabled scaling animations for app tiles.
- Code housekeeping to reuse mark-up templates and mixins.

### Fixed
- onTap events in Android devices.
- Swipe and iScroll confliting with diagonal scrolling.
- Removed old browser prefixes.
Beeapp React
============

A mobile portfolio built with [React](https://facebook.github.io/react/), the Facebook JavaScript library for building user interfaces.

## Web Performance Results
----
Beeapp has been tested against different tools to analyse and optimize the web app following web best practices. These are the most recent results:

- [Web Page Test](http://www.webpagetest.org/result/150915_G3_1C8W/)
- [Google PageSpeed Insights](https://developers.google.com/speed/pagespeed/insights/?url=http%3A%2F%2Fwww.beeapp.io)

Our next steps will focus on using a CDN for all static assets and leverage browser caching of [Parse](https://parse.com/) static assets.